	// create the module and name it scotchApp
	var scotchApp = angular.module('scotchApp', ['ngRoute']);

	// configure our routes
	scotchApp.config(function($routeProvider) {
		$routeProvider

			// route for the home page
			.when('/', {
				templateUrl : 'pages/home.html',
				controller  : 'mainController'
			})

			// route for the about page
			.when('/about', {
				templateUrl : 'pages/about.html',
				controller  : 'aboutController'
			})

			// route for the contact page
			.when('/contact', {
				templateUrl : 'pages/contact.html',
				controller  : 'contactController'
			})
			
			// route for the education page
			.when('/edu', {
				templateUrl : 'pages/edu.html',
				controller  : 'eduController'
			})
			
			// route for the work page
			.when('/work', {
				templateUrl : 'pages/work.html',
				controller  : 'workController'
			})
			
			// route for the publication page
			.when('/pub', {
				templateUrl : 'pages/pub.html',
				controller  : 'pubController'
			})
			
			// route for the experience page
			.when('/exp', {
				templateUrl : 'pages/exp.html',
				controller  : 'expController'
			});
	});

	// create the controller and inject Angular's $scope
	scotchApp.controller('mainController', function($scope) {
		// create a message to display in our view
		$scope.message = 'Everyone come and see how good I look!';
	});

	scotchApp.controller('aboutController', function($scope) {
		$scope.message = 'Look! I am an about page.';
	});

	scotchApp.controller('contactController', function($scope) {
		$scope.message = 'Contact us! JK. This is just a demo.';
	});
	
	scotchApp.controller('eduController', function($scope) {
		$scope.message = 'University of Florida!~Education Demo';
	});
	
	scotchApp.controller('expController', function($scope) {
		$scope.message = 'Halo E-Cigs!~Experience Demo.';
	});
	
	scotchApp.controller('workController', function($scope) {
		$scope.message = 'Some Awesome Works here.';
	});
	
	scotchApp.controller('pubController', function($scope) {
		$scope.message = 'Publications.';
	});
	
